$(document).ready(function(){
    $( function() {
        $( ".accordion" ).accordion();
      } );
    
      $('.accordion').accordion({  //biar pas loading pertama ketutup semua
        active: false,
        collapsible: true            
     });

    $('#change').click(function(){
        $('body').toggleClass('dark');
        $('button').toggleClass('dark');
        $('.textBox').toggleClass('textBoxDark');
        $('.accordion').toggleClass('accordionDark');
    });

});