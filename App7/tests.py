from django.test import TestCase, Client,LiveServerTestCase
from django.urls import resolve

from .views import App7views

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest (TestCase):
    def test_url_can_be_accessed(self) :
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_calls_app7views (self) :
        response = resolve('/')
        self.assertEqual(response.func, App7views)
    
    def test_url_use_app7html (self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'appHtml/app7.html')
    
    def test_html_file_contains_scripts_tag (self) :
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('<script', content)

    def test_html_file_contains_button_tag (self) :
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('<button', content)
        self.assertIn('Change the Theme', content)
    
    def test_html_contains_accordion(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('accordion', content)

    def test_html_contains_jquery(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", content)

class FunctionalTest (LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)

        #cari elemen
        button = selenium.find_element_by_id('change')
        aktivitas = selenium.find_element_by_name('aktivitas')
        organisasi = selenium.find_element_by_name('organisasi')
        prestasi = selenium.find_element_by_name('prestasi')
        # klik tombol tema dark
        button.send_keys(Keys.RETURN)
        aktivitas.send_keys(Keys.RETURN)
        organisasi.send_keys(Keys.RETURN)
        prestasi.send_keys(Keys.RETURN)
